import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';

  ngOnInit() {
    console.log('Cart-UI service: localStorage auth_key is: ' + window.localStorage.getItem('auth_key-homepage'));
    console.log('Cart-UI service: sessionStorage auth_key is: ' + window.sessionStorage.getItem('auth_key-homepage'));
  }

}
